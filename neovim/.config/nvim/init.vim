call plug#begin()

" Only in Neovim:
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'sbdchd/neoformat'
Plug 'https://gitlab.com/yorickpeterse/vim-paper.git'

call plug#end()

color paper

let mapleader = " "
let maplocalleader = " "

nnoremap <leader>f :Files<CR>

" Sets
set termguicolors
set relativenumber
