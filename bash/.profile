test -z "$PROFILEREAD" && . /etc/profile || true

export PS1="\w \n> "
export PATH="${HOME}/.local/bin:$PATH"
export PATH="${HOME}/.screenlayout:$PATH"
export PATH="$PATH:/usr/local/go/bin"
export GOPATH="$HOME/dev/go"
export GOBIN="$HOME/dev/go/bin"
export CGO_ENABLED=1
export PATH="$PATH:$(go env GOPATH)/bin"
# macOS cross compiler
export PATH="$PATH:/home/kenzie/dev/osxcross/target/bin"
export FZF_DEFAULT_COMMAND='fd --type f --follow --exclude .git -E "*.class" -E "*.hex"'

export PATH="$HOME/flutter/bin:$PATH"
export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"
export PATH=$PATH:$HOME/.gem/ruby/2.6.0/bin

# Fix for intellij IDEAVIM locking keyboard
export XMODIFIERS=""
export IBUS_ENABLE_SYNC_MODE=1
export MOZ_USE_XINPUT2=1
export JAVA_HOME=/usr/lib64/jvm/jre-1.8.0

[[ -f ~/.bash_custom ]] && source ~/.bash_custom
