#
# ~/.bashrc
#
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
EDITOR=nvim

[[ -f ~/.bash_aliases ]] && source ~/.bash_aliases
[[ -f ~/.bash_functions ]] && source ~/.bash_functions
[[ -f ~/.bash_custom ]] && source ~/.bash_functions
[[ -f ~/.profile ]] && source ~/.profile
[ -f ~/.fzf.bash ] && source ~/.fzf.bash
