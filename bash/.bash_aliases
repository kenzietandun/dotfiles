alias c='xclip -selection clipboard -i'
alias ll='ls -alh'
alias t='/home/kenzie/dev/nyaa-up/venv/bin/python3 /home/kenzie/dev/nyaa-up/downloader.py'
alias tunnelssh='ssh -D 0.0.0.0:8001 -N'
alias s='source venv/bin/activate'
alias x="xdg-open"
alias wn="ssh -X vulcan 'whatnext'"
alias wake="sudo etherwake -i wlp3s0 e0:d5:5e:87:ba:ff"
alias fel="flutter emulators --launch Pixel_3a_API_30_x86"
alias ap="ansible-playbook -i hosts"
alias vm="sudo virsh net-start default && sudo virsh start debian10 && cd ~/dev/ && sleep 30 && sshfs dev:mrimpossible work"

