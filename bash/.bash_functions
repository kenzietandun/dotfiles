#!/bin/bash

kk() {
    KB="$1"
    cd ~/dev/qmk-keymaps/qmk_firmware && \
    echo 'Removing old json files' && \
    rm -f ./*_rev1_layout_mine.json && \
    echo 'Backing up new json file from downloads folder to respective keyboard folder' && \
    cp ~/Downloads/*_rev1_layout_mine.json ../"$KB" && \
    echo 'Moving json file to qmk folder' && \
    mv ~/Downloads/*_rev1_layout_mine.json ./ && \
    echo 'Generating keymap.c' && \
    qmk json2c *_rev1_layout_mine.json > ../"$1"/keymap.c && \
    cd ../  && \
    make "$1"
}

tl() {
    streamlink \
        --player mpv \
        $1 best
}

i() {
    sudo ddcutil --display 1 setvcp 10 + 25
    sudo ddcutil --display 2 setvcp 10 + 25
}

d() {
    sudo ddcutil --display 1 setvcp 10 - 25
    sudo ddcutil --display 2 setvcp 10 - 25
}


dlnvim() {
    curl -LO https://github.com/neovim/neovim/releases/download/nightly/nvim.appimage && \
    chmod u+x nvim.appimage && \
    sudo mv nvim.appimage /usr/local/bin/nvim
}

# function to quickly rebuild python venv
# useful when there is a python version update
rebuild_python() {
  REPO_URL=$(git -C ./src/ remote -v | grep fetch | awk '{print $2}')
  DIR=$(echo "${REPO_URL}" | cut -d '/' -f 2 | cut -d '.' -f 1)
  cd ~/git/
  rm -rf "${DIR}"
  mkdir "${DIR}"
  cd "${DIR}"
  python3 -m venv .
  git clone "${REPO_URL}" src
  source bin/activate
  pip install -r src/req.txt
}



gogh() {
  bash -c "$(wget -qO- https://git.io/vQgMr)"
}

clone() {
  git clone git@gitlab.com:kenzietandun/"$1"
}

cd() {
  builtin cd "$@"
  if [[ -d "venv" ]]; then
    source venv/bin/activate
  fi
}



mkfdir() {
    TARGET="$1"
    CURR_DIR="$(basename ${PWD})"
    mkdir -p "${TARGET}"
    touch "${TARGET}/${TARGET}.dart"
    echo "export '';" > "${TARGET}/${TARGET}.dart"
    if [[ -f "${CURR_DIR}.dart" ]]
    then
        echo "export '${TARGET}/${TARGET}.dart';" >> "${CURR_DIR}.dart"
    elif [[ -f "${CURR_DIR}s.dart" ]]
    then
        echo "export '${TARGET}/${TARGET}.dart';" >> "${CURR_DIR}s.dart"
    fi
}

ss() {
    CLIENT="$1"
    HOSTNAME=$(dig "$CLIENT".gymmasteronline.com +short | head -n 1 | cut -d '.' -f 1)
    ssh "${HOSTNAME}"
}

sscp() {
    FILENAME="$1"
    CLIENT="$2"
    HOSTNAME=$(dig "$CLIENT".gymmasteronline.com +short | head -n 1 | cut -d '.' -f 1)
    scp "${FILENAME}" "${HOSTNAME}":/tmp/
}

sscpr() {
    FILENAME="$1"
    CLIENT="$2"
    HOSTNAME=$(dig "$CLIENT".gymmasteronline.com +short | head -n 1 | cut -d '.' -f 1)
    scp "${HOSTNAME}":/tmp/"${FILENAME}" ./
}
